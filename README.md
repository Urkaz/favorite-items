# Character Sheet Favorites

This module is designed to insert a display of favorite items in the dnd5e character sheet for quick access.

![example image](documentation/favorite-items_preview.webp)
![demo video](documentation/favorite-items_demo.webm)

## Supported systems

Only [dnd5e](https://foundryvtt.com/packages/dnd5e) is supported currently.  I'm not opposed to integration into other systems, but I have no plans to do so either.  Feel free to submit a PR if you would like to add integration for another system.  

## Installation

Module manifest: https://gitlab.com/mxzf/favorite-items/-/releases/permalink/latest/downloads/module.json


## Credits

The underying idea for a favorites list is from [Sky's Alternate 5th Edition Dungeons & Dragons Sheet](https://foundryvtt.com/packages/alt5e)

Most of the code for the templates was copied from the [dnd5e system](https://foundryvtt.com/packages/dnd5e) and then chopped up and tweaked to fit.  
