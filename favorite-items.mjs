Hooks.on('setup', () => {
    game.settings.register('favorite-items', 'favorite-icon', {
        name: 'FAVORITE-ITEMS.FavIcon',
        hint: 'FAVORITE-ITEMS.FavIconDesc',
        scope: 'client',
        config: true,
        type: String,
        default: 'fa-heart',
        choices: {
            'fa-circle': 'Circle',
            'fa-heart': 'Heart',
            'fa-star': 'Star',
            'fa-sun': 'Sun',
        },
    })
    game.settings.register('favorite-items', 'favorite-color', {
        name: 'FAVORITE-ITEMS.FavColor',
        hint: 'FAVORITE-ITEMS.FavColorDesc',
        scope: 'client',
        config: true,
        type: String,
        default: '#c53131',
        //onChange: value => {
        //  console.log(value)
        //},
      });
    // Ensure template is loaded so that it will be ready when needed
    loadTemplates(['modules/favorite-items/templates/dnd5e_character.hbs'])
})

Hooks.on('renderSettingsConfig', (app, el, data) => {
    // Insert color picker input
    el.find('[name="favorite-items.favorite-color"]').parent()
      .append(`<input type="color" value="${game.settings.get('favorite-items','favorite-color')}" data-edit="favorite-items.favorite-color">`)
    // Insert preview icon
    el.find('[data-tab="favorite-items"] h2')
      .append(` - <i class="favorite-preview fa-solid ${game.settings.get('favorite-items','favorite-icon')}" style="color: ${game.settings.get('favorite-items','favorite-color')}"></i>`)//.css({'color':'red'})
});

Hooks.on('renderActorSheet5eCharacter', (app, html, data) => {
    let favs = app.actor.items.filter(i => i.getFlag('favorite-items','favorite'))

    // Apply the toggle state info (mostly for equiped gear and prepared spells)
    //favs.forEach(i => app._prepareItemToggleState(i))
    
    let context = {
        'owner': data.owner,
        'inventory': favs.filter(i => ['weapon','equipment','consumable','tool','backpack','loot'].includes(i.type)),
        'features': favs.filter(i => ['feat','background','class','subclass'].includes(i.type)),
        'spells': app._prepareSpellbook({'actor':app.actor},favs.filter(i => i.type==='spell')),
        'itemContext': favs.reduce((obj, i) => {obj[i.id] ??= {hasUses: i.system.uses && (i.system.uses.max>0)}; app._prepareItemToggleState(i, obj[i.id]); return obj}, {})
    }
    
    // Insert flag so that the spell section can decide not to render, because it'll never be an empty list
    context.hasSpells = context.spells.some(level => level.spells.length > 0)
    
    // Skip rendering the favorites section if there are no favorites to render
    if (favs.length>0) {
        // Render the context with the template and inject it after the counters in the tab
        const inject = _templateCache['modules/favorite-items/templates/dnd5e_character.hbs'](context, {allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true})
        //const inject = await renderTemplate('modules/favorite-items/templates/dnd5e_character.hbs', context)
        html.find('.counters').after(inject)
        
        // Activate the assorted core listeners on the newly inserted rows
        html.find(".favorites .item .item-name.rollable h4").click(event => app._onItemSummary(event));
        if (data.owner){
            html.find(".favorites .item-toggle").click(app._onToggleItem.bind(app));
            html.find(".favorites .item-edit").click(app._onItemEdit.bind(app));
            html.find(".favorites .item-delete").click(app._onItemDelete.bind(app));
            html.find(".favorites .item-uses input").click(ev => ev.target.select()).change(app._onUsesChange.bind(app));
            html.find(".favorites .rollable .item-image").click(event => app._onItemUse(event));
        } else {
            html.find(".favorites .rollable").each((i, el) => el.classList.remove("rollable"));
        }
    }

    // Insert favorite icons in item pages
    html.find('.item .item-controls:not(.effect-controls)').prepend(`<a class="item-control item-favorite" title="${game.i18n.localize('FAVORITE-ITEMS.Favorite')}"><i class="fa-solid ${game.settings.get('favorite-items','favorite-icon')}"></i></a>`)

    // Recolor any icons matching an item that's favorited
    for (let item of favs.map(i => i.id)) {
        html.find(`[data-item-id="${item}"]`).find('.item-favorite').css({'color':game.settings.get('favorite-items','favorite-color')})
        //html.find(`[data-item-id="${item}"]`).find('.item-favorite').addClass('favorite')
    }
    
    // Set up handling for the icon to toggle the favorite flag
    html.find('.item-favorite').on('click', (event) => {
        const item_id = event.currentTarget.closest('[data-item-id]').dataset.itemId
        const item = app.actor.items.get(item_id)
        item.setFlag('favorite-items','favorite',!item.getFlag('favorite-items','favorite'))
    })
    
})